import React, { Component } from 'react';
import {
  AppRegistry,
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Dimensions,
} from 'react-native';
import update from 'immutability-helper';
import ActionSheet from 'react-native-actionsheet'

export default class MainScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: this._genItems()
    };
  };

  componentDidUpdate() {
    this.refs.flatList.scrollToEnd();
  };

  _genItems() {
    var dataBlob = [];
    for (var i = 0; i < 10; i++) {
      dataBlob.push('Game ' + (i + 1));
    }
    return dataBlob;
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.listContainer}>
          <FlatList
            ref='flatList'
            data={this.state.dataSource}
            keyExtractor={(item, index) => item}      
            renderItem={({item, index}) => this._renderItem(item, index)}
            numColumns={3}
          />
        </View>
        <View style={styles.bottomContainer}>
          <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.button} onPress={() => this._addGame()}>
              <Text style={styles.buttonText}>Add new game</Text>
            </TouchableOpacity>
          </View>
        </View>
        <ActionSheet
            ref={o => this.ActionSheet = o}
            title='Delete this game?'
            options={['Cancel', 'Delete']}
            cancelButtonIndex={0}
            destructiveButtonIndex={1}
            onPress={(index) => this._handleDeleteGame(index)}
          />
      </View>
    );
  };

  _renderItem(item, index) {
    var imgSource = {
      uri: 'https://pictures.abebooks.com/isbn/9789577316547-us.jpg',
    };

    return (
      <TouchableOpacity onPress={() => {this._pressItem(index)}} onLongPress={() => this._showActionSheet(index)}>
        <View>
          <View style={styles.row}>
            <Image style={styles.thumb} source={imgSource} />
            <Text style={styles.text}>
              {item}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  _pressItem(index) {
    console.log('You click item ' + index);
  };

  _addGame() {
    var newDataSource = update(this.state.dataSource, {$push: ['Game ' + (this.state.dataSource.length + 1)]});
    this.setState({dataSource: newDataSource});
  };

  _showActionSheet(index) {
    this._selectedIndex = index
    this.ActionSheet.show()
  };

  _handleDeleteGame(index) {
    if (index === this.ActionSheet.props.destructiveButtonIndex) {
      var newDataSource = update(this.state.dataSource, {$splice: [[this._selectedIndex, 1]]});
      this.setState({dataSource: newDataSource});
    }
  };

};

var {height, width} = Dimensions.get('window');
var rowWidth = (width - 60) / 3;
var thumbWidth = rowWidth - 32;
var styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    alignItems: 'stretch',
    flexDirection: 'column',
    flex: 1,
    backgroundColor: '#cccccc',
  },
  listContainer: {
    flex: 5,
  },
  bottomContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  buttonContainer: {
    flex: 1,
    paddingHorizontal: 10,
  },
  button: {
    borderColor: '#dadada',
    borderRadius: 8,
    borderWidth: 2,
    backgroundColor: '#303030',
    height: 59,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: '#fafafa',
    fontSize: 16,
    fontWeight: '400',
    textAlign: 'center',
  },
  row: {
    justifyContent: 'center',
    padding: 5,
    margin: 10,
    width: rowWidth,
    height: rowWidth,
    backgroundColor: '#F6F6F6',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#CCC'
  },
  thumb: {
    width: thumbWidth,
    height: thumbWidth
  },
  text: {
    flex: 1,
    marginTop: 5,
    fontWeight: 'bold'
  },
});
