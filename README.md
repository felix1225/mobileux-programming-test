# README #

## Guide to run ##

1. Make sure you have installed latest version of Xcode and Android Studio on your Mac.
2. Follow this [instruction](https://facebook.github.io/react-native/docs/getting-started.html) to config android emulator. (Select tab "Building Projects with Native Code" with "macOS" and "Android")
3. Navigate to root folder of source code - the folder includes 'index.ios.js' and 'index.android.js' in terminal.
4. Run command 'npm install' in terminal.
5. Run command 'react-native run-ios' if test on iOS.
6. Launch android emulator you created in step 2 and run command 'react-native run-android' if test on android.

## Instructions ##

* Click button "Add new game" to add a new game entry.
* Long press game item to delete that.